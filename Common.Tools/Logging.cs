﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Tools
{
    public class Logging
    {

        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger("SystemLogger");

        public Logging()
        { }

        public void WatcherCreated(string message)
        {
            if (message == "Start")
            {
                logger.Info("-------------------------------------------------");
                logger.Info("------------Watcher Created Started--------------");
                logger.Info("-------------------------------------------------");
            }
            else if (message == "End")
            {
                logger.Info("-------------------------------------------------");
                logger.Info("------------Watcher Created Ended----------------");
                logger.Info("-------------------------------------------------");
            }

        }

        public void FileInputs(string filename)
        {
            logger.Info("INPUTTED FILE: " + filename);
        }

        public void ExceptionProcess(string ex, string header)
        {
            logger.Info("Exception Encountered: " + ex);
            logger.Info("Process : " + header); 
        }

        public void SuccessProcess(string fileName)
        {
            logger.Info("Success on Process! : " + fileName);
        }

        public void DirectLog(string filename)
        {
            logger.Info(filename);
        }
    }
}
