﻿using Common.Tools;
using FileImportExport;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FileHandler
{
    public class ServiceController
    {
        FileSystemWatcher fileWatcher;
        string dirPath1 = ConfigurationManager.AppSettings["DirectoryPath1"];
        string dirPath2 = ConfigurationManager.AppSettings["DirectoryPath2"];
        Logging Log = new Logging();

        public ServiceController()
        {
            fileWatcher = new FileSystemWatcher();
            fileWatcher.Created += new FileSystemEventHandler(watcher_FileFetch);
        }

        public void Start()
        {
            DirectoryValidation();
            fileWatcher.Path = Path.GetDirectoryName(dirPath1);
            fileWatcher.EnableRaisingEvents = true;
            fileWatcher.IncludeSubdirectories = true;
            fileWatcher.NotifyFilter = NotifyFilters.FileName | NotifyFilters.DirectoryName;

        }
        public void Stop()
        {
            fileWatcher.EnableRaisingEvents = false;
        }

        private void DirectoryValidation()
        {
            try
            {

                if (!Directory.Exists(dirPath1))
                {
                    EventLog.WriteEntry("OnStart - DirectoryValidation()", "ERROR: AutoStagging doesnt exist. Creating directory " + dirPath1);
                    Directory.CreateDirectory(dirPath1);
                }

                if (!Directory.Exists(dirPath2))
                {
                    EventLog.WriteEntry("OnStart - DirectoryValidation()", "ERROR: AutoStagging doesnt exist. Creating directory " + dirPath2);
                    Directory.CreateDirectory(dirPath2);
                }

            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("OnStart - DirectoryValidation()", "Encountered and Error Exception : " + ex.Message);
                
            }
        }

        void watcher_FileFetch(object sender, FileSystemEventArgs e)
        {
            if (e.ChangeType == WatcherChangeTypes.Created)
            {

                string fn = e.FullPath;
                Log.FileInputs(Path.GetFileName(fn));

                int i = 0;
                while (!IsFileReady(e.FullPath))
                {
                    if (!File.Exists(e.FullPath))
                        return;
                    Thread.Sleep(300);
                    i = i + 1;
                }

               EventLog.WriteEntry("ServiceController - watcher_FileFetch", "Importing in Progress!");

                try
                {
                    FileTransfers ft = new FileTransfers();
                    ft.FileExport(fn, dirPath2 + Path.GetFileName(fn));
                    Thread.Sleep(200);
                    Log.SuccessProcess(Path.GetFileName(fn));
                }
                catch (System.Exception ex)
                {
                    EventLog.WriteEntry("FileHandler - Exception - watcher_FileFetch", "Exception Message :" + ex.Message); 
                    Log.ExceptionProcess("FileHandler - Exception - watcher_FileFetch.  Exception Message :", ex.Message);
                }

            }


        }
        bool IsFileReady(string filename)
        {
            FileInfo fi = new FileInfo(filename);
            FileStream fs = null;
            try
            {
                fs = fi.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
                return true;
            }
            catch (IOException)
            {
                return false;
            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }

      


    }
    }
