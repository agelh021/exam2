﻿using Common.Tools;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileImportExport
{
    public class FileTransfers
    {
        Logging Log = new Logging();
        public FileTransfers()
        { }
        public void FileExport(string sourcePath, string destPath)
        {
            try
            {
                File.Move(sourcePath, destPath);
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("FileExport - Exception Encountered", "Exception Message :" + ex.Message);
                Log.ExceptionProcess("FileExport - Exception Encountered .  Exception Message :", ex.Message);
            }
            
        }

        //public void FileImport(string sourcePath, string destPath)
        //{

        //}


    }
}
