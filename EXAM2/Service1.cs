﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace EXAM2
{
    public partial class Exam2Default : ServiceBase
    {
        FileHandler.ServiceController sCon = new FileHandler.ServiceController();
        public Exam2Default()
        {
            InitializeComponent();
            sCon.Start();
        }

        protected override void OnStart(string[] args)
        {
        }

        protected override void OnStop()
        {
        }
    }
}
